//input: số có 2 chữ số
/*progress:
tách chữ số hàng chục = làm tròn (số nhập vào chia cho 10)
tách chữ số hàng đơn vị = số nhập vào chia dư cho 10
tính tổng 2 chữ số vừa tách được
*/
//output: tổng của 2 chữ số

function sumDigits() {
  // Lấy giá trị số từ ô input
  var number = parseInt(document.getElementById("number").value);
  var resultNumber = document.getElementById("resultNumber");
  // Tính tổng 2 ký số
  var tensDigit = Math.floor(number / 10);
  var onesDigit = number % 10;
  var sum = tensDigit + onesDigit;

  // Hiển thị kết quả lên màn hình
  resultNumber.innerHTML = "Tổng 2 ký số của " + number + " là: " + sum;
  resultNumber.style.display = "block";
}
