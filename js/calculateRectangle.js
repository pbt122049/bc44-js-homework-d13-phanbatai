//input: chiều dài, chiều rộng
//progress: diện tích = dài x rộng, chu vi = (dài + rộng) x 2
//output: diện tích và chu vi

function calculateRectangle() {
  // Lấy giá trị chiều dài và chiều rộng
  var length = parseFloat(document.getElementById("length").value);
  var width = parseFloat(document.getElementById("width").value);
  var resultRectangle = document.getElementById("resultRectangle");
  // Tính diện tích chu vi
  var area = length * width;
  var perimeter = (length + width) * 2;
  // Hiện kết quả ra màn hình
  resultRectangle.textContent = `Diện tích hình chữ nhật là ${area}, chu vi hình chữ nhật là ${perimeter}.`;
  resultRectangle.style.display = "block";
}
