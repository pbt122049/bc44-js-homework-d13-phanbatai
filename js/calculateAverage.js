//input: giá trị 5 số từ ô input
/*progress:
Tính tổng cộng của 5 số = số 1 + số 2 + số 3 + số 4 + số 5
lấy tổng của 5 số chia cho 5
*/
//output: Hiển thị kết quả trung bình cộng lên màn hình

function calculateAverage() {
  // Lấy giá trị 5 số từ ô input
  var number1 = parseFloat(document.getElementById("number1").value);
  var number2 = parseFloat(document.getElementById("number2").value);
  var number3 = parseFloat(document.getElementById("number3").value);
  var number4 = parseFloat(document.getElementById("number4").value);
  var number5 = parseFloat(document.getElementById("number5").value);
  var resultAverage = document.getElementById("resultAverage");
  // Tính trung bình cộng
  var sum = number1 + number2 + number3 + number4 + number5;
  var average = sum / 5;

  // Hiển thị kết quả trung bình cộng lên màn hình
  resultAverage.innerHTML = "Trung bình cộng của 5 số là: " + average;
  resultAverage.style.display = "block";
}
